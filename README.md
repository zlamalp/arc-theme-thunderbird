# Arc-Theme-Thunderbird

* Works best with Thunderbird until 52.7.0
* Personal version of Monterail theme, which respect system theming
  * Forked from: https://github.com/spymastermatt/thunderbird-monterail
* Contains fixes to match correctly Arc-like GTK theme of Firefox.
  * Best with Arc-Dark theme, for light or darker some fixed values must be changed.

## Changes

* Use system font and style.
* Smaller rows, match folder list height.
* Use square tabs, even for calendar views.
* Recolored some icons (green->orange folders etc.).
* Modified style for menubar / toolbar to match Arc theme style, where
  in a case of Arc-Dark menubar is dark and toolbar is light.
* Same style applied to addressbook and message composing window.

## Installation

* Copy whole content (userChrome.css and all folders) into /home/[username]/.thunderbird/[profile]/chrome/
  * If chrome folder doesn't exist, just create it.

## Screenshots

![alt text](https://gitlab.com/zlamalp/arc-theme-thunderbird/raw/master/screen.png)

![alt text](https://gitlab.com/zlamalp/arc-theme-thunderbird/raw/master/screen_2.png)

![alt text](https://gitlab.com/zlamalp/arc-theme-thunderbird/raw/master/screen_3.png)